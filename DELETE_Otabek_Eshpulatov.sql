do
$$
    DECLARE
        film_id_     INTEGER;
        rental_id_   INTEGER;
        customer_id_ INTEGER;
        id           INTEGER;
    BEGIN

        -- retrieving film id
        select film_id
        into film_id_
        from film
        where title = 'Avengers: Infinity War'
          and release_year = 2018
          and original_language_id = 1;


-- deleting rental data if it's associated with the film I added before
        delete
        from rental
        where inventory_id = ((select inv.inventory_id from inventory inv where inv.film_id = film_id_))
        returning rental_id into rental_id_;

-- deleting payment details also because it directly corresponds to rental
        delete from payment where rental_id = rental_id_;


        -- deleting film actors

        for id in ((select actor_id from film_actor where film_id = film_id_))
            loop
                --  deleting actors
                delete from film_actor where actor_id = id and film_id = film_id_;
                delete from actor where actor_id = id;
            end loop;


        delete from inventory where film_id = film_id_;
        delete from film_category where film_id = film_id_;

-- deleting film
        delete from film where film_id = film_id_;


        select customer_id into customer_id_ from customer where email = 'otabek.eshpulatov@gmail.com';

        delete from payment where customer_id = customer_id_;
        delete from rental where customer_id = customer_id_;

    END
$$;
